package com.olx.exercise.account.action;


import java.util.Optional;

import com.olx.exercise.app.error.AccountNotFoundException;
import com.olx.exercise.account.repository.AccountRepository;
import com.olx.exercise.account.model.Account;

public class AccountAction {

	private AccountRepository accountRepository;

	public AccountAction(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	public Account getAccountById(long accountId) {
		Optional<Account> account = accountRepository.getAccountById(accountId);
		return account.orElseThrow(() -> new AccountNotFoundException("Account " + accountId + " not found"));
	}

	public void saveAccount(Account testAccount) {
		accountRepository.saveAccount(testAccount);
	}
}

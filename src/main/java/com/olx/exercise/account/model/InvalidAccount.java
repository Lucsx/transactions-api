package com.olx.exercise.account.model;

public class InvalidAccount implements AccountState {

	public static final String INVALID = "Invalid";

	@Override
	public boolean isValid() {
		return false;
	}

	@Override
	public String getState() {
		return INVALID;
	}
}

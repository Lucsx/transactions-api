package com.olx.exercise.account.model;

public class ValidAccount implements AccountState {

	public static final String VALID = "Valid";

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public String getState() {
		return VALID;
	}
}

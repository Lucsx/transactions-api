package com.olx.exercise.account.model;

import com.olx.exercise.app.error.NotEnoughCurrencyInOriginAccountException;

public class Account {
	private long id;
	private String bank;
	private String country;
	private double balance;
	private AccountState accountState;

	public Account(long id, String bank, String country) {
		this.id = id;
		this.bank = bank;
		this.country = country;
		this.accountState = new ValidAccount();
	}

	private Account(long id) {
		this.id = id;
		this.accountState = new InvalidAccount();
	}

	public static Account createValidAccount(long id, String bank, String country) {
		return new Account(id, bank, country);
	}

	public static Account createInvalidAccountWithId(long id) {
		return new Account(id);
	}

	public long getId() {
		return id;
	}

	public String getBank() {
		return bank;
	}

	public String getCountry() {
		return country;
	}

	public double getBalance() {
		return balance;
	}

	public boolean isFormSameCountryAs(Account targetAccount) {
		return targetAccount.isFromCountry(country);
	}

	private boolean isFromCountry(String country) {
		return this.country.equals(country);
	}

	public boolean isFromSameBankAs(Account targetAccount) {
		return targetAccount.isFromBank(bank);
	}

	private boolean isFromBank(String bank) {
		return this.bank.equals(bank);
	}

	public boolean isValid() {
		return accountState.isValid();
	}

	public void withdrawAmount(double amount) {
		if(balance < amount) {
			throw new NotEnoughCurrencyInOriginAccountException();
		}
		 balance -= amount;
	}

	public void withdrawFee(double fee) {
		balance -= fee;
	}

	public void fundWith(double amount) {
		balance += amount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Account account = (Account) o;
		return id == account.id;
	}

	public boolean canWithdraw(double amount) {
		return balance < amount;
	}

	public String getState() {
		return accountState.getState();
	}
}

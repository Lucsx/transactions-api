package com.olx.exercise.account.model;

public interface AccountState {
	boolean isValid();
	String getState();
}

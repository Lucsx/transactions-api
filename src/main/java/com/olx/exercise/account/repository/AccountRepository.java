package com.olx.exercise.account.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.olx.exercise.app.error.AccountAlreadyExistsException;
import com.olx.exercise.account.model.Account;

public class AccountRepository {
	private List<Account> accounts;

	public AccountRepository() {
		this.accounts = new ArrayList<>();
	}

	public synchronized Optional<Account> getAccountById(long testAccountId) {
		return accounts
				.stream()
				.filter(account -> account.getId() == testAccountId)
				.findFirst();
	}

	public synchronized void updateAccount(Account account) {
		accounts.remove(account);
		accounts.add(account);
	}

	public synchronized void saveAccount(Account accountToSave) {
		if (existsAccountWithId(accountToSave.getId())) {
			throw new AccountAlreadyExistsException();
		}
		accounts.add(accountToSave);
	}

	private synchronized boolean existsAccountWithId(long accountId) {
		return accounts.stream().anyMatch(account -> account.getId() == accountId);
	}
}

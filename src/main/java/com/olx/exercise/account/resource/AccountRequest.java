package com.olx.exercise.account.resource;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountRequest {
	@JsonProperty("id")
	private long id;

	@JsonProperty("bank")
	private String bank;

	@JsonProperty("country")
	private String country;

	public long getId() {
		return id;
	}

	public String getBank() {
		return bank;
	}

	public String getCountry() {
		return country;
	}

}

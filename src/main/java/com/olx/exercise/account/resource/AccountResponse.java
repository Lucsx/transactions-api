package com.olx.exercise.account.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.olx.exercise.account.model.Account;

public class AccountResponse {
	@JsonProperty("id")
	private long id;

	@JsonProperty("bank")
	private String bank;

	@JsonProperty("country")
	private String country;

	@JsonProperty("balance")
	private double balance;

	private AccountResponse(long id, String bank, String country, double balance) {
		this.id = id;
		this.bank = bank;
		this.country = country;
		this.balance = balance;
	}

	public static AccountResponse createFromAccount(Account testAccount) {
		return new AccountResponse(testAccount.getId(), testAccount.getBank(), testAccount.getCountry(), testAccount.getBalance());
	}

	public long getId() {
		return id;
	}

	public String getBank() {
		return bank;
	}

	public String getCountry() {
		return country;
	}

	public double getBalance() {
		return balance;
	}

}

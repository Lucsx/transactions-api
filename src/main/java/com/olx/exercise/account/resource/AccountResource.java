package com.olx.exercise.account.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.olx.exercise.account.action.AccountAction;
import com.olx.exercise.account.model.Account;

@Path("/api/account")
@Consumes("application/json")
@Produces("application/json")
public class AccountResource {
	private AccountAction accountAction;
	private long testAccountId;

	public AccountResource(AccountAction accountAction, long testAccountId) {
		this.accountAction = accountAction;
		this.testAccountId = testAccountId;
	}

	@GET
	public Response getAccount() {
		Account testAccount = accountAction.getAccountById(testAccountId);
		return Response.ok(AccountResponse.createFromAccount(testAccount)).build();
	}
}

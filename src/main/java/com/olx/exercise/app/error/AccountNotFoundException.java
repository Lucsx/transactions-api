package com.olx.exercise.app.error;

public class AccountNotFoundException extends RuntimeException {

	public AccountNotFoundException (String message) { super(message); }

}

package com.olx.exercise.app.config;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.olx.exercise.account.model.Account;
import com.olx.exercise.app.config.TestAccountConfiguration;

import io.dropwizard.Configuration;

public class TransactionsConfiguration extends Configuration {
	@JsonProperty("initialPoolSize")
	@NotNull
	private int initialPoolSize;

	@JsonProperty("testAccount")
	@NotNull
	private TestAccountConfiguration testAccountConfiguration;

	@JsonProperty("filePath")
	@NotNull
	private String filePath;

	public int getInitialPoolSize() {
		return initialPoolSize;
	}

	public Account getTestAccountConfiguration() {
		return TestAccountConfiguration.createAccount(testAccountConfiguration);
	}

	public String getFilePath() {
		return filePath;
	}
}

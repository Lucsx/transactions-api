package com.olx.exercise.app.config;

import com.codahale.metrics.health.HealthCheck;

public class TransactionsHealthCheck extends HealthCheck {
	protected Result check() throws Exception {
		return Result.healthy();
	}
}

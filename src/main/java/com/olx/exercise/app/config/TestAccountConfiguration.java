package com.olx.exercise.app.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.olx.exercise.account.model.Account;

public class TestAccountConfiguration {
	@JsonProperty("id")
	private long id;

	@JsonProperty("bank")
	private String bank;

	@JsonProperty("country")
	private String country;

	@JsonProperty("balance")
	private double balance;

	public long getId() {
		return id;
	}

	public String getBank() {
		return bank;
	}

	public String getCountry() {
		return country;
	}

	public double getBalance() {
		return balance;
	}

	public static Account createAccount(TestAccountConfiguration testAccountConfiguration) {
		return Account.createValidAccount(testAccountConfiguration.getId(), testAccountConfiguration.getBank(), testAccountConfiguration.getCountry());
	}
}

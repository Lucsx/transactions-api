package com.olx.exercise.app;

import com.olx.exercise.account.action.AccountAction;
import com.olx.exercise.account.repository.AccountRepository;
import com.olx.exercise.account.resource.AccountResource;
import com.olx.exercise.app.config.TransactionsConfiguration;
import com.olx.exercise.app.config.TransactionsHealthCheck;
import com.olx.exercise.transaction.action.TransactionFileFactory;
import com.olx.exercise.transaction.action.TransactionsAction;
import com.olx.exercise.transaction.resource.TransactionResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class TransactionsApplication extends Application<TransactionsConfiguration> {
	private static final String HEALTHCHECK_APP = "transactions_healthcheck";

	private TransactionsAction transactionsAction;
	private AccountAction accountAction;

	public static void main(String[] args) throws Exception {
		new TransactionsApplication().run(args);
	}

	@Override
	public void run(TransactionsConfiguration configuration, Environment environment) {
		registerHealthChecks(environment);
		createActions(configuration);
		registerResources(environment, configuration);
		createInitialTestAccount(configuration);
	}

	private void createInitialTestAccount(TransactionsConfiguration configuration) {
		accountAction.saveAccount(configuration.getTestAccountConfiguration());
	}

	private void registerResources(Environment environment, TransactionsConfiguration transactionsConfiguration) {
		environment.jersey().register(new TransactionResource(transactionsAction));
		environment.jersey().register(new AccountResource(accountAction, transactionsConfiguration.getTestAccountConfiguration().getId()));
	}

	private void createActions(TransactionsConfiguration configuration) {
		AccountRepository accountRepository = new AccountRepository();
		TransactionFileFactory transactionFileFactory = new TransactionFileFactory(configuration.getFilePath());
		this.transactionsAction = new TransactionsAction(transactionFileFactory, accountRepository, configuration.getInitialPoolSize());

		this.accountAction = new AccountAction(accountRepository);
	}

	private void registerHealthChecks(Environment environment) {
		environment.healthChecks().register(HEALTHCHECK_APP, new TransactionsHealthCheck());
	}
}

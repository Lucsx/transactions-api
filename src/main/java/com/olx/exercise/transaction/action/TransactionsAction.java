package com.olx.exercise.transaction.action;

import java.io.FileOutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.olx.exercise.account.model.Account;
import com.olx.exercise.account.repository.AccountRepository;
import com.olx.exercise.transaction.model.Transaction;

public class TransactionsAction {
	private AccountRepository accountRepository;
	private ExecutorService executorService;
	private BlockingQueue<Transaction> queue;

	public TransactionsAction(TransactionFileFactory transactionFileFactory, AccountRepository accountRepository, int initialPoolSize) {
		FileOutputStream outStream = transactionFileFactory.getOutStream();
		this.accountRepository = accountRepository;
		this.executorService = Executors.newFixedThreadPool(initialPoolSize);

		this.queue = new LinkedBlockingQueue<>();
		new Thread(new TransactionProcessingService(outStream, queue)).start();
	}

	public void processNewTransaction(long sourceAccountId, long targetAccountId, double amount) {
		executorService.execute(() -> {
			Transaction transaction = buildTransaction(sourceAccountId, targetAccountId, amount);
			updateTransactionAccounts(transaction);
			queue.add(transaction);
		});
	}

	private void updateTransactionAccounts(Transaction transaction) {
		synchronized (transaction.getSourceAccount()) {
			synchronized (transaction.getTargetAccount()) {
				if(transaction.isValid()) {
					double amount = transaction.getAmount();
					withdrawAmountAndFeeFromAccount(transaction.getSourceAccount(), amount, transaction.calcularImpuesto());
					fundAccountWithAmount(transaction.getTargetAccount(), amount);
				}
			}
		}
	}

	private void fundAccountWithAmount(Account account, double amount) {
		account.fundWith(amount);
		accountRepository.updateAccount(account);
	}

	private void withdrawAmountAndFeeFromAccount(Account account, double amount, double fee) {
		account.withdrawAmount(amount);
		account.withdrawFee(fee);
		accountRepository.updateAccount(account);
	}

	private Transaction buildTransaction(long sourceAccountId, long targetAccountId, double amount) {
		Account sourceAccount = accountRepository.getAccountById(sourceAccountId).orElse(Account.createInvalidAccountWithId(sourceAccountId));
		Account targetAccount = accountRepository.getAccountById(targetAccountId).orElse(Account.createInvalidAccountWithId(targetAccountId));
		return Transaction.create(sourceAccount, targetAccount, amount);
	}

}

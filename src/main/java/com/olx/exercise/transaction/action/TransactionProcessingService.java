package com.olx.exercise.transaction.action;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import com.olx.exercise.transaction.model.Transaction;

public class TransactionProcessingService implements Runnable {
	private FileOutputStream outputStream;
	private final BlockingQueue<Transaction> queue;

	public TransactionProcessingService(FileOutputStream outputStream, BlockingQueue<Transaction> queue) {
		this.outputStream = outputStream;
		this.queue = queue;
	}

	@Override
	public void run() {
			while (true) {
					try {
						try {
							Transaction transaction = queue.take();
							recordTransactionState(transaction);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
	}

	private void recordTransactionState(Transaction transaction) throws IOException {
		outputStream.write(transaction.getState().getBytes("UTF-8"));
	}

}

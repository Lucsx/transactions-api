package com.olx.exercise.transaction.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class TransactionFileFactory {

	private FileOutputStream outStream;

	public TransactionFileFactory(String filePath) {
		createFile(filePath);
	}

	private void createFile(String filePath) {
		File file = new File(filePath);
		try {
			outStream = new FileOutputStream(file, true);
		}
		catch (FileNotFoundException e) {
				e.printStackTrace();
		}
	}

	public FileOutputStream getOutStream() {
		return outStream;
	}
}

package com.olx.exercise.transaction.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.olx.exercise.account.model.Account;
import com.olx.exercise.account.resource.AccountRequest;
import com.olx.exercise.transaction.action.TransactionsAction;

@Path("/api/transaction")
@Consumes("application/json")
@Produces("application/json")
public class TransactionResource {

	private TransactionsAction transactionsAction;

	public TransactionResource(TransactionsAction transactionsAction) {
		this.transactionsAction = transactionsAction;
	}

	@POST
	public Response processTransaction(TransactionRequest transactionRequest) {
		long sourceAccountId = transactionRequest.getSourceAccount();
		long targetAccountId = transactionRequest.getTargetAccount();
		double amount = transactionRequest.getAmount();

		if(sourceAccountId == targetAccountId) {
			return Response.status(400).build();
		}

		transactionsAction.processNewTransaction(sourceAccountId, targetAccountId, amount);
		return Response.ok().build();
	}
}

package com.olx.exercise.transaction.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.olx.exercise.account.resource.AccountRequest;

public class TransactionRequest {
	@JsonProperty("cuentaOrigen")
	private long sourceAccountId;

	@JsonProperty("cuentaDestino")
	private long targetAccountId;

	@JsonProperty("montoATransferir")
	private double amount;

	public double getAmount() {
		return amount;
	}

	public long getTargetAccount() {
		return targetAccountId;
	}

	public long getSourceAccount() {
		return sourceAccountId;
	}
}

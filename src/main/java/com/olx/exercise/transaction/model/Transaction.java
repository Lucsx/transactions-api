package com.olx.exercise.transaction.model;

import java.util.UUID;
import com.olx.exercise.account.model.Account;

public class Transaction {
	private final Account sourceAccount;
	private final Account targetAccount;
	private final double amount;
	private TransactionType transactionType;
	private String id;
	private TransactionState transactionState;

	private Transaction(Account sourceAccount, Account targetAccount, double amount) {
		this.id = UUID.randomUUID().toString();
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.amount = amount;
		this.transactionType = TransactionTypeFactory.create(areAccountsFromSameCountry(), areAccountsFromSameBank());
		this.transactionState = new ValidTransaction();
	}

	private Transaction(Account sourceAccount, Account targetAccount, double amount, TransactionState transactionState) {
		this.id = UUID.randomUUID().toString();
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.amount = amount;
		this.transactionState = transactionState;
	}

	public double calcularImpuesto() {
		return transactionType.getFee() * amount;
	}

	public static Transaction create(Account sourceAccount, Account targetAccount, double amount) {
		return TransactionValidator
				.getMessageWhenInvalid(sourceAccount, targetAccount, amount)
				.map((errorMessage) -> createInvalidTransaction(sourceAccount, targetAccount, amount, errorMessage))
				.orElse(new Transaction(sourceAccount, targetAccount, amount));
	}

	private static Transaction createInvalidTransaction(Account sourceAccount, Account targetAccount, double amount, String errorMessage) {
		return new Transaction(sourceAccount, targetAccount, amount, new InvalidTransaction(errorMessage));
	}

	private boolean areAccountsFromSameCountry(){
		return sourceAccount.isFormSameCountryAs(targetAccount);
	}

	private boolean areAccountsFromSameBank(){
		return sourceAccount.isFromSameBankAs(targetAccount);
	}

	public Account getSourceAccount() {
		return sourceAccount;
	}

	public Account getTargetAccount() {
		return targetAccount;
	}

	public double getAmount() {
		return amount;
	}

	public String getState() {
		return "The transaction " + id + " is " + transactionState.getState() + ". " + transactionState.verbose();
	}

	public boolean isValid() {
		return transactionState.isValid();
	}
}

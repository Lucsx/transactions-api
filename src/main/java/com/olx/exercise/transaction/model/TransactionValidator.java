package com.olx.exercise.transaction.model;

import java.util.Optional;

import com.olx.exercise.account.model.Account;

public class TransactionValidator {

	public static Optional<String> getMessageWhenInvalid(Account sourceAccount, Account targetAccount, double amount) {
		Optional<String> message = Optional.empty();
		if(!sourceAccount.isValid() || !targetAccount.isValid()) {
			message = Optional.of(
					"Source account: " + sourceAccount.getId() + " is " + sourceAccount.getState() + "." + "Target account: " + targetAccount.getId()
							+ " is " + targetAccount.getState() + ".");
		}
		if(!sourceAccount.canWithdraw(amount)) {
			message = Optional.of("Balance is not enough in source account: " + sourceAccount.getId());
		}
		return message;
	}
}

package com.olx.exercise.transaction.model;

public class InternationalTransaction implements TransactionType {
	public static final double INTERNATIONAL_TRANSACTION_FEE = 0.05;
	private boolean isSameBank;

	public InternationalTransaction(boolean isSameBank) {
		this.isSameBank = isSameBank;
	}

	public double getFee() {
		return INTERNATIONAL_TRANSACTION_FEE;
	}
}

package com.olx.exercise.transaction.model;

public class ValidTransaction implements TransactionState {

	private static final String VALID = "Valid";

	@Override
	public String verbose() {
		return "";
	}

	@Override
	public String getState() {
		return VALID;
	}

	@Override
	public boolean isValid() {
		return true;
	}
}

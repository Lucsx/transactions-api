package com.olx.exercise.transaction.model;

public interface TransactionState {

	String verbose();

	String getState();

	boolean isValid();
}

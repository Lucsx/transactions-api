package com.olx.exercise.transaction.model;

public class TransactionTypeFactory {

	public static TransactionType create(boolean isNationalTransaction, boolean areAccountsFromSameBank) {
		if(isNationalTransaction) {
			return new NationalTransaction(areAccountsFromSameBank);
		} else {
			return new InternationalTransaction(areAccountsFromSameBank);
		}
	}
}

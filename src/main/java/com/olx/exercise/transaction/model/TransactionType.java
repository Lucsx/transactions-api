package com.olx.exercise.transaction.model;

public interface TransactionType {

	double getFee();
}

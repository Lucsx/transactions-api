package com.olx.exercise.transaction.model;

public class InvalidTransaction implements TransactionState{

	private static final String INVALID = "Invalid";
	private String errorMessage;

	public InvalidTransaction(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String verbose() {
		return errorMessage;
	}

	@Override
	public String getState() {
		return INVALID;
	}

	@Override
	public boolean isValid() {
		return false;
	}
}

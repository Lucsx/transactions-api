package com.olx.exercise.transaction.model;

public class NationalTransaction implements TransactionType {
	public static final double NATIONAL_TRANSACTION_FEE = 0.01;
	public static final int NON_FEE = 0;
	private boolean isSameBank;

	public NationalTransaction(boolean isSameBank) {
		this.isSameBank = isSameBank;
	}

	public double getFee() {
		return isSameBank ? NON_FEE : NATIONAL_TRANSACTION_FEE;
	}
}

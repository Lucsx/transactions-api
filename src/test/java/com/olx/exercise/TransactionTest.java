package com.olx.exercise;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import com.olx.exercise.account.model.Account;
import com.olx.exercise.transaction.model.Transaction;

public class TransactionTest {

	private static final double EXPECTED_INTERNATIONAL_FEE = 0.05;
	private static final double EXPECTED_NATIONAL_FEE = 0.01;
	private static final double NON_FEE = 0;
	private static final int ACCOUNT_ID = 1;
	private static final int ANOTHER_ACCOUNT_ID = 2;
	private static final String BANK = "bank1";
	private static final String ANOTHER_BANK = "bank2";
	private static final double AMOUNT = 100;

	@Test
	public void whenTransactionIsInternationalAndFromDifferentBanksCalcularImpuestoReturns5PercentFee() {
		//GIVEN
		Transaction transaction = givenAnInternationalTransactionFromBanks(BANK, ANOTHER_BANK);
		//WHEN
		double fee = transaction.calcularImpuesto();
		//THEN
		assertThat(fee).isEqualTo(EXPECTED_INTERNATIONAL_FEE * transaction.getAmount());
	}

	@Test
	public void whenTransactionIsNationalAndFromDifferentBanksCalcularImpuestoReturns1PercentFee() {
		//GIVEN
		Transaction transaction = givenNationalTransactionFromBanks(BANK, ANOTHER_BANK);
		//WHEN
		double fee = transaction.calcularImpuesto();
		//THEN
		assertThat(fee).isEqualTo(EXPECTED_NATIONAL_FEE * transaction.getAmount());
	}

	@Test
	public void whenTransactionIsNationalAndFromSameBankCalcularImpuestoReturns0PercentFee() {
		//GIVEN
		Transaction transaction = givenNationalTransactionFromBanks(BANK, BANK);
		//WHEN
		double fee = transaction.calcularImpuesto();
		//THEN
		assertThat(fee).isEqualTo(NON_FEE * transaction.getAmount());
	}

	@Test
	public void whenTransactionIsInternationalAndFromSameBankCalcularImpuestoReturns5PercentFee() {
		//GIVEN
		Transaction transaction = givenAnInternationalTransactionFromBanks(BANK, BANK);
		//WHEN
		double fee = transaction.calcularImpuesto();
		//THEN
		assertThat(fee).isEqualTo(EXPECTED_INTERNATIONAL_FEE * transaction.getAmount());
	}

	private Transaction givenNationalTransactionFromBanks(String bank1, String bank2) {
		String country = "country1";
		Account sourceAccount = Account.createValidAccount(ACCOUNT_ID, bank1, country);
		Account targetAccount = Account.createValidAccount(ANOTHER_ACCOUNT_ID, bank2, country);
		return Transaction.create(sourceAccount, targetAccount, AMOUNT);
	}

	private Transaction givenAnInternationalTransactionFromBanks(String bank1, String bank2) {
		Account sourceAccount = Account.createValidAccount(ACCOUNT_ID, bank1, "country1");
		Account targetAccount = Account.createValidAccount(2, bank2, "country2");
		return Transaction.create(sourceAccount, targetAccount, AMOUNT);
	}
}
